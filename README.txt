INTRODUCTION
------------

Drupal 8 replaced Taxonomy Term references with Entity References. When
managing the form display you can choose between four widgets provided by core
that are generic for all Entity References. This module provides widgets
specifically for Term references.

REQUIREMENTS
------------

This module requires the following modules:

 * Field UI (Drupal core)
 * Taxonomy (Drupal core)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module.

CONFIGURATION
-------------

 * Create an Entity Reference field that references Taxonomy Terms.
 * Edit the form display mode of that specific entity type to change the widget
 of the field.

MAINTAINERS
-----------

Current maintainers:
 * Bart Vanhoutte (Bart Vanhoutte) - https://www.drupal.org/user/1133754

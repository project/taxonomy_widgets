<?php

namespace Drupal\taxonomy_widgets\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\OptGroup;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Trait TaxonomyWidgetsTrait.
 *
 * Provides common functionality for taxonomy widgets.
 *
 * @package Drupal\taxonomy_widgets\Plugin\Field\FieldWidget.
 */
trait TaxonomyWidgetsTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $settings['max_depth'] = NULL;
    $settings['flatten'] = NULL;
    $settings += parent::defaultSettings();
    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    if (!isset($this->options)) {
      $options = [];

      /* @var $entityManager \Drupal\Core\Entity\EntityTypeManagerInterface. */
      $entityManager = \Drupal::service('entity.manager');
      $settings = $this->fieldDefinition->getSettings();
      $maxDepth = $this->getSetting('max_depth');
      $bundles = array_values($settings['handler_settings']['target_bundles']);
      $numberOfBundles = count($bundles);

      foreach ($bundles as $bundle) {
        if ($vocabulary = Vocabulary::load($bundle)) {

          /* @var $storage Drupal\taxonomy\TermStorageInterface. */
          $storage = $entityManager->getStorage('taxonomy_term');
          if ($terms = $storage->loadTree($vocabulary->id(), 0, $maxDepth, TRUE)) {
            foreach ($terms as $term) {

              $label = $entityManager->getTranslationFromContext($term)->label();

              // Don't add a prefix when flatten is enabled.
              if (!$this->getSetting('flatten')) {
                $label = str_repeat('-', $term->depth) . $label;
              }

              // Don't add option groups when there's only one bundle.
              if ($numberOfBundles === 1) {
                $options[$term->id()] = $label;
              }
              else {
                $options[$vocabulary->label()][$term->id()] = $label;
              }

            }
          }
        }
      }

      // Add an empty option if the widget needs one.
      if ($empty_label = $this->getEmptyLabel()) {
        $options = ['_none' => $empty_label] + $options;
      }

      // Let other modules modify the options.
      $module_handler = \Drupal::moduleHandler();
      $context = [
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $entity,
      ];
      $module_handler->alter('options_list', $options, $context);

      // Sanitize all labels.
      array_walk_recursive($options, [$this, 'sanitizeLabel']);

      if (!$this->supportsGroups()) {
        $options = OptGroup::flattenOptions($options);
      }

      $this->options = $options;
    }

    return $this->options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['max_depth'] = [
      '#type' => 'number',
      '#title' => $this->t('Max depth'),
      '#description' => $this->t('The number of levels of the tree to return.'),
      '#min' => 1,
      '#default_value' => $this->getSetting('max_depth'),
    ];

    $element['flatten'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Flatten'),
      '#description' => t('Flattens the vocabulary hierarchy.'),
      '#default_value' => $this->getSetting('flatten'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    if ($maxDepth = $this->getSetting('max_depth')) {
      $summary[] = t('Max depth: @max_depth', [
        '@max_depth' => $maxDepth,
      ]);
    }

    if ($this->getSetting('flatten')) {
      $summary[] = t('Flatten: yes');
    }

    return $summary;
  }

}

<?php

namespace Drupal\taxonomy_widgets\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Taxonomy Select widget.
 *
 * Provides a widget for taxonomy term fields that can be configured with a
 * maximum depth.
 *
 * @FieldWidget(
 *   id = "taxonomy_widgets_select_widget",
 *   label = @Translation("Taxonomy Widgets: select list"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class TaxonomyWidgetsSelectWidget extends OptionsSelectWidget {

  use TaxonomyWidgetsTrait;

}

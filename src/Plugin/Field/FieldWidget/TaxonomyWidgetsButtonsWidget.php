<?php

namespace Drupal\taxonomy_widgets\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Taxonomy Buttons widget.
 *
 * Provides a widget for term fields that can be configured with a
 * maximum depth.
 *
 * @FieldWidget(
 *   id = "taxonomy_widgets_buttons_widget",
 *   label = @Translation("Taxonomy Widgets: check boxes/radio buttons"),
 *   field_types = {
 *     "entity_reference",
 *   },
 *   multiple_values = TRUE
 * )
 */
class TaxonomyWidgetsButtonsWidget extends OptionsButtonsWidget {

  use TaxonomyWidgetsTrait;

}
